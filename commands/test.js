module.exports = {
	name: 'test',
	aliases: ['test2', 'test3'],
	cooldown: 0,
	description: 'Assist in developing',
  usage: '<user> <role>',
	serverOnly: true,
  args: true,
	execute(message, args) {
    if (args[0] === 'foo') {
			return message.channel.send('bar');
		}

		message.channel.send(`Arguments: ${args}\nArguments length: ${args.length}`);
	},
};
