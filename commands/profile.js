module.exports = {
	name: 'profile',
	description: 'Provides the player/players profile',
	execute(message, args) {
		var profile = "Discord ID: \n";
		profile += "Discord Username: \n";
		profile += "Discord Nickname: \n";
		profile += "League Username: \n";
		profile += "League Rank: \n";
		profile += "Game Status: \n";
		profile += "Time Zone: \n";
		profile += "Note: \n";
		message.channel.send(profile);
	},
};
