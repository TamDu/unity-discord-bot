//File System library
const fs = require('fs');
const Discord = require('discord.js');
//Note this contains private information, so has not been added to bitbucket
const auth = require('./auth.json');
//Note: {param} creates a variable instead of an object
const config = require('./config.json');
const client = new Discord.Client();


client.commands = new Discord.Collection();
//Set cooldowns for commands
const cooldowns = new Discord.Collection();
//Get all command files
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
//Initialize the commands & execution
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
	//Checks if the command is from a user and if it starts with proper prefix
	//Ignores bot commands from bots
  if (!msg.content.startsWith(config.prefix) || msg.author.bot){
    return;
  }
	//formats the command
  const args = msg.content.slice(config.prefix.length).split(/ +/);
  const commandName = args.shift().toLowerCase();
	//Checks if the command exsists & gets the command and validates the arguements
	const command = client.commands.get(commandName)
		|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	if (!command) return;

	//validates message
	if (command.args && !args.length) {
		let reply = `You didn't provide any arguments, ${msg.author}!`;

		if (command.usage) {
			reply += `\nThe proper usage would be: \`${config.prefix}${command.name} ${command.usage}\``;
		}

		return msg.channel.send(reply);
	}

	//Checks if the command can only be used on a server.
	if (command.serverOnly && msg.channel.type !== 'text') {
		return msg.reply('I can\'t execute that command inside DMs!');
	}
	//Check for or set cooldowns
	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	//Note: only if cooldown is undefined it defaults to default_cooldown.
	const cooldownAmount = (command.cooldown || config.default_cooldown) * 1000;
	if (timestamps.has(msg.author.id)) {
		const expirationTime = timestamps.get(msg.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return msg.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
		}
	}
	timestamps.set(msg.author.id, now);
	//setTimeout is a default function that setTimeout(function(){}, time in millisecond)
	setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount);

	try {
    command.execute(msg, args);
  } catch (error) {
    console.error(error);
  }
});

client.login(auth.token);
